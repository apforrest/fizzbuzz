/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package org.forrest.fizzbuzz.impl;

import org.forrest.fizzbuzz.Globals;
import org.forrest.fizzbuzz.Pipe;
import org.forrest.fizzbuzz.Processor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author andrew.forrest@forgerock.com
 */
public class FileProcessor implements Processor {

    private static final String FILE_NAME = "/" + System.currentTimeMillis() + "-%d.txt";

    private final List<Pipe<String>> pipes;
    private final CountDownLatch finished;
    private final Writer writer;

    public FileProcessor(List<Pipe<String>> pipes, CountDownLatch finished, int order) {
        this.pipes = pipes;
        this.finished = finished;

        try {
            String fullPath = Globals.INSTANCE.getOutputDirectory() + FILE_NAME;
            File file = new File(String.format(fullPath, order));
            writer = new BufferedWriter(new FileWriter(file), Globals.INSTANCE.getWriteBufferSize());
        } catch (IOException ioE) {
            throw new IllegalStateException("File creation failed.", ioE);
        }
    }

    @Override
    public void run() {
        try {
            for (Pipe<String> pipe : pipes) {

                // Initial pull.
                String value = pipe.pull();

                while (!Globals.END_MARKER.equals(value)) {
                    writer.write(value);
                    value = pipe.pull();
                }
            }
        } catch (IOException ioE) {
            throw new IllegalStateException("File writing failed.", ioE);
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                // Swallow up.
            }
        }

        // Notify processor has complete.
        finished.countDown();
    }

}
