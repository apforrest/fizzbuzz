/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package org.forrest.fizzbuzz.impl;

import org.forrest.fizzbuzz.Globals;
import org.forrest.fizzbuzz.Pipe;
import org.forrest.fizzbuzz.Processor;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author andrew.forrest@forgerock.com
 */
public class DumpProcessor implements Processor {

    private final List<Pipe<String>> pipes;
    private final CountDownLatch finished;

    public DumpProcessor(List<Pipe<String>> pipes, CountDownLatch finished) {
        this.pipes = pipes;
        this.finished = finished;
    }

    @Override
    public void run() {
        for (Pipe<String> pipe : pipes) {

            // Initial pull.
            String value = pipe.pull();

            while (!Globals.END_MARKER.equals(value)) {
                // Do nothing with the value, this is a dump implementation.
                value = pipe.pull();
            }
        }

        finished.countDown();
    }

}
