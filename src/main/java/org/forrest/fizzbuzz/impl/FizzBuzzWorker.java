package org.forrest.fizzbuzz.impl;/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */

import org.forrest.fizzbuzz.Globals;
import org.forrest.fizzbuzz.Pipe;
import org.forrest.fizzbuzz.Worker;

/**
 * @author andrew.forrest@forgerock.com
 */
public class FizzBuzzWorker implements Worker {

    private static final String FIZZ = "fizz\n";
    private static final String BUZZ = "buzz\n";
    private static final String FIZZBUZZ = "fizzbuzz\n";
    private static final char NEW_LINE = '\n';

    private final int min;
    private final int max;
    private Pipe<String> pipe;

    public FizzBuzzWorker(int min, int max, Pipe<String> pipe) {
        this.min = min;
        this.max = max;
        this.pipe = pipe;
    }

    @Override
    public void run() {
        int batchUpSize = Globals.INSTANCE.getBatchUpSize();
        StringBuilder buffer = new StringBuilder();

        for (int count = min; count <= max; count++) {
            if (count % batchUpSize == 0) {
                pipe.put(buffer.toString());
                buffer = new StringBuilder();
            }

            if (count % 15 == 0) {
                buffer.append(FIZZBUZZ);
                continue;
            }

            if (count % 3 == 0) {
                buffer.append(BUZZ);
                continue;
            }

            if (count % 5 == 0) {
                buffer.append(FIZZ);
                continue;
            }

            buffer.append(count).append(NEW_LINE);
        }

        if (buffer.length() > 0) {
            // Push out any data left in the buffer.
            pipe.put(buffer.toString());
        }

        // Close down the pipe.
        pipe.put(Globals.END_MARKER);
    }

}
