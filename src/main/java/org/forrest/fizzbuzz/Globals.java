/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package org.forrest.fizzbuzz;

import com.beust.jcommander.Parameter;

/**
 * @author andrew.forrest@forgerock.com
 */
public enum Globals {

    INSTANCE;

    public static final String END_MARKER = "!";

    @Parameter(names = {"-maxValue", "-v"}, description = "The FizzBuzz maximum value")
    private int maxValue = 1000000000;

    @Parameter(names = {"-maxThreads", "-t"}, description = "The maximum number of threads")
    private int maxThreads = 22;

    @Parameter(names = {"-divisions", "-d"}, description = "How big the divisions work should be")
    private int divisions = 800000;

    @Parameter(names = {"-maxPipeSize", "-q"}, description = "The maximum pipe size a worker can fill")
    private int maxQueueSize = 5000;

    @Parameter(names = {"-batchUpSize", "-b"}, description = "The batch size before writing to the pipe")
    private int batchUpSize = 60;

    @Parameter(names = {"-writeBufferSize", "-s"}, description = "The write buffer size")
    private int writeBufferSize = 8192;

    @Parameter(names = {"-outputDirectory", "-o"}, description = "The output directory", required = true)
    private String outputDirectory;

    @Parameter(names = {"-dumpMode", "-m"}, description = "Dump mode does not write to disk (output directory is ignored)")
    private boolean dumpMode = false;

    @Parameter(names = {"-workersPerProcessor", "-w"}, description = "The number of workers to have allocated to each processor")
    private int workersPerProcessor = 1;

    public int getWorkersPerProcessor() {
        return workersPerProcessor;
    }

    public boolean isDumpMode() {
        return dumpMode;
    }

    public int getWriteBufferSize() {
        return writeBufferSize;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public int getBatchUpSize() {
        return batchUpSize;
    }

    public int getMaxQueueSize() {
        return maxQueueSize;
    }

    public int getDivisions() {
        return divisions;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public int getMaxValue() {
        return maxValue;
    }

}
