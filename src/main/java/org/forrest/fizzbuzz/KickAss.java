/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package org.forrest.fizzbuzz;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.forrest.fizzbuzz.impl.DumpProcessor;
import org.forrest.fizzbuzz.impl.FileProcessor;
import org.forrest.fizzbuzz.impl.FizzBuzzWorker;
import org.forrest.fizzbuzz.impl.ProcessFactoryImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author andrew.forrest@forgerock.com
 */
public class KickAss {

    public static void main(String[] args) {
        Globals globals = Globals.INSTANCE;
        JCommander commander = new JCommander(globals);
        commander.setProgramName("KickAss FizzBuzz");

        try {
            commander.parse(args);
            new KickAss().start(globals);
        } catch (ParameterException pE) {
            System.out.println(pE.getMessage());
            commander.usage();
        }
    }

    public void start(Globals globals) {
        // Take initial timing.
        long startTime = System.nanoTime();

        int maxValue = globals.getMaxValue();
        int divisions = globals.getDivisions();
        int workerCount = (int)Math.ceil(maxValue / (divisions * 1d));

        int workerPoolSize = (globals.getMaxThreads() + 1) / 2;
        int processorPoolSize = globals.getMaxThreads() - workerPoolSize;

        // Initiate executor services.
        ExecutorService workerExecutor = Executors.newFixedThreadPool(workerPoolSize);
        ExecutorService processorExecutor = Executors.newFixedThreadPool(processorPoolSize);

        List<Pipe<String>> pipes = new ArrayList<Pipe<String>>(workerCount);

        // Initialise and kick off the worker threads.
        for (int min = 1, max = 0; min <= maxValue; min += divisions) {
            max = (min + divisions < maxValue) ? min + divisions : maxValue;
            Pipe<String> pipe = new BlockingPipe<String>();
            workerExecutor.submit(new FizzBuzzWorker(min, max, pipe));
            pipes.add(pipe);
        }

        ProcessorFactory factory = new ProcessFactoryImpl();
        int workersPerProcessor = globals.getWorkersPerProcessor();
        int processorCount = (int)Math.ceil(workerCount / (workersPerProcessor * 1d));
        CountDownLatch finished = new CountDownLatch(processorCount);

        // Initialise and kick off the processor threads.
        for (int start = 0, end = 0; start < workerCount; start += workersPerProcessor) {
            end = (start + workersPerProcessor < workerCount) ? start + workersPerProcessor : workerCount;
            processorExecutor.submit(factory.getProcessor(pipes.subList(start, end), finished));
        }

        try {
            // Wait on all processors to complete.
            finished.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        // Close down the executors.
        workerExecutor.shutdown();
        processorExecutor.shutdown();

        // Print out final timing.
        System.out.println(String.format("Time taken is %d nanoseconds.", System.nanoTime() - startTime));
    }

}
